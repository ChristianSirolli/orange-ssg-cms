#!/usr/bin/env python3
'''
MIT License

Copyright (c) 2020 Christian Sirolli

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''
# Install and import dependencies
import subprocess, sys
def install(package):
    subprocess.check_call([sys.executable, "-m", "pip", "install", package])
install('markdown2')
install('pyyaml')
import os, json, yaml, shutil, markdown2, re, collections.abc
from pathlib import Path
print(os.getcwd())
def update(d, u):
    for k, v in u.items():
        if isinstance(v, collections.abc.Mapping):
            d[k] = update(d.get(k, {}), v)
        else:
            d[k] = v
    return d
# Define variables
config = {}
netlifyCMSScript = '<script src="https://identity.netlify.com/v1/netlify-identity-widget.js"></script></head><body>'
pbScript = '<script src="https://orange-ssg-cms.netlify.com/pb.js"></script>'
top = '<!doctype html><html><head><meta charset="utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1.0" /><title>'
bottom = '</script></body></html>'
fileContents = {
    'admin/blog/index.html': top+'Content Manager</title>'+netlifyCMSScript+'<script src="https://unpkg.com/netlify-cms/dist/netlify-cms.js">'+bottom,
    'admin/editor/index.html': top+'Page Editor</title>'+pbScript+netlifyCMSScript+'<script src="https://orange-ssg-cms.netlify.com/grapesjs.js">'+bottom,
    'admin/index.html': top+'OrangeCMS Dashboard</title>'+pbScript+netlifyCMSScript+'<script src="https://orange-ssg-cms.netlify.com/orange-cms.js">'+bottom,
}
config = {
    "url": "https://example.com",
    "branch": "master",
    "git": "gitlab",
    "grapesjs": True,
    "netlifycms": True,
    "blog": {
        'backend': {
            'name': 'git-gateway',
            'branch': 'master'
        },
        'publish_mode': 'editorial_workflow',
        'site_url': '',
        'media_folder': 'assets',
        'collections': [{
            'name': 'blog',
            'label': 'Blog Posts',
            'label_singular': 'Post',
            'folder': 'posts',
            'create': True,
            'slug': '{{year}}-{{month}}-{{day}}-{{slug}}',
            'fields': [
                {'label': 'Layout', 'name': 'layout', 'widget': 'hidden', 'default': 'post'},
                {'label': 'Title', 'name': 'title', 'widget': 'string'},
                {'label': 'Publish Date', 'name': 'date', 'widget': 'datetime'},
                {'label': 'Featured Image', 'name': 'thumbnail', 'widget': 'image', 'required': False},
                {'label': 'Body', 'name': 'body', 'widget': 'markdown'}
            ]
        }]
    },
    "cms": {
        "path": "public",
        "ignore": [],
        "debugLvl": "error",
        "gitType": 'gitlab',
        "masterBranch": 'master',
        "devBranch": "orange-dev"
    },
    "include": [],
    "exclude": []
}
pages = []
assets = []
parts = []
patterns = []
posts = []
html_pages = {}
md_pages = {}
html_parts = {}
md_parts = {}
html_patterns = {}
md_patterns = {}
html_posts = {}
md_posts = {}
# Get configuration
with open('./.orange-config.json', 'r') as confile:
    update(config, json.load(confile))
    confile.close()
    update(config, {
        'cms': {
            "gitType": config['git'],
            "masterBranch": config['branch'],
        },
        'blog': {
            'backend': {
                'branch': config['branch']
            },
            'site_url': config['url']
        }
    })
# Organize files into dictionaries
for f in list(os.walk('assets')):
    for i in range(0, len(f[2])):
        f[2][i] = os.path.join(f[0], f[2][i])
    assets.extend(f[2])
for f in list(os.walk('pages')):
    for i in range(0, len(f[2])):
        f[2][i] = os.path.join(f[0], f[2][i])
    pages.extend(f[2])
for f in list(os.walk('parts')):
    for i in range(0, len(f[2])):
        f[2][i] = os.path.join(f[0], f[2][i])
    parts.extend(f[2])
for f in list(os.walk('patterns')):
    print('walk (pt):', f)
    for i in range(0, len(f[2])):
        f[2][i] = os.path.join(f[0], f[2][i])
    patterns.extend(f[2])
print(patterns)
for f in list(os.walk('posts')):
    for i in range(0, len(f[2])):
        f[2][i] = os.path.join(f[0], f[2][i])
    posts.extend(f[2])
for f in parts:
    with open(f, 'r') as f2:
        f2c = f2.read()
        fm = {}
        if re.match(r'~~~([\s\S]*?)~~~', f2c, re.M):
            m = list(filter(lambda x: x != "", re.match(r'~~~\n*((?:\w+?: *[ \S]*?\n)+?)~~~', f2c, re.M).group(1).split('\n')))
            for i in m:
                a = i.split(':')
                fm[a[0].strip()] = a[1].strip()
        fm['body'] = re.sub(r'~~~([\s\S]*?)~~~', '', f2c, flags=re.M)
    if re.search(r'\.html$', f):
        html_parts[f] = fm
    if re.search(r'\.md$', f):
        md_parts[f] = fm
for f in pages:
    print('(pg):', f)
    with open(f, 'r') as f2:
        f2c = f2.read()
        if re.match(r'~~~\n*((?:\w+?: *[ \S]*?\n)+?)~~~', f2c, re.M):
            m = list(filter(lambda x: x != "", re.match(r'~~~\n*((?:\w+?: *[ \S]*?\n)+?)~~~', f2c, re.M).group(1).split('\n')))
            fm = {}
            for i in m:
                a = i.split(':')
                fm[a[0].strip()] = a[1].strip()
            fm['body'] = re.sub(r'~~~([\s\S]*?)~~~', '', f2c, flags=re.M)
            if re.search(r'\.html$', f):
                html_pages[f] = fm
                print('Initial (h-pg):', f, html_pages[f])
            if re.search(r'\.md$', f):
                md_pages[f] = fm
                print('Initial (m-pg):', f, md_pages[f])
for f in patterns:
    print('(pt):', f)
    with open(f, 'r') as f2:
        f2c = f2.read()
        fm = {}
        if re.match(r'~~~\n*((?:\w+?: *[ \S]*?\n)+?)~~~', f2c, re.M):
            m = list(filter(lambda x: x != "", re.match(r'~~~\n*((?:\w+?: *[ \S]*?\n)+?)~~~', f2c, re.M).group(1).split('\n')))
            for i in m:
                a = i.split(':')
                fm[a[0].strip()] = a[1].strip()
        fm['body'] = re.sub(r'~~~([\s\S]*?)~~~', '', f2c, flags=re.M)
    if re.search(r'\.html$', f):
        html_patterns[f] = fm
        print('Initial (h-pt):', f, html_patterns[f])
    if re.search(r'\.md$', f):
        md_patterns[f] = fm
        print('Initial (m-pt):', f, md_patterns[f])
for f in posts:
    with open(f, 'r') as f2:
        f2c = f2.read()
        fm = {}
        if re.match(r'~~~([\s\S]*?)~~~', f2c, re.M):
            m = list(filter(lambda x: x != "", re.match(r'~~~\n*((?:\w+?: *[ \S]*?\n)+?)~~~', f2c, re.M).group(1).split('\n')))
            for i in m:
                a = i.split(':')
                fm[a[0].strip()] = a[1].strip()
        fm['body'] = re.sub(r'~~~([\s\S]*?)~~~', '', f2c, flags=re.M)
    if re.search(r'\.html$', f):
        html_posts[f] = fm
    if re.search(r'\.md$', f):
        md_posts[f] = fm
# Make directories
os.mkdir('./public')
os.mkdir('./public/admin')
os.mkdir('./public/assets')
os.mkdir('./public/posts')
# Helper defs to keep DRY
def mdToHTML(fm):
    fm['body'] = markdown2.markdown(fm['body'])
    return fm
def replVars(body, fm):
    t = body
    for m in re.findall(r'(?<!\{)\{{2}\s+(.*?)\s+\}{2}(?!\})', body, flags=re.M):
        t = re.sub(r'(?<!\{)\{{2}\s+.*?\s+\}{2}(?!\})', fm[m], t, flags=re.M)
    return t
def mixParts(body):
    t = body
    for m in re.findall(r'(?<!\{)\{{3}\s+(.*?)\s+\}{3}(?!\})', body, flags=re.M):
        t = re.sub(r'(?<!\{)\{{3}\s+.*?\s+\}{3}(?!\})', html_parts[os.path.join('parts', re.sub(r'\.(html|md)$', '', m) + '.html')]['body'], t, flags=re.M)
        print('mixParts:', t)
    return t
def mixPatterns(body, pattern):
    t = body
    p = html_patterns[os.path.join('patterns', re.sub(r'\.(html|md)$', '', pattern) + '.html')]['body']
    for m in re.findall(r'(?<!\{)(\{{3}\s+body\s+\}{3})(?!\})', p, flags=re.M):
        t = re.sub(r'(?<!\{)\{{3}\s+body\s+\}{3}(?!\})', t, p, flags=re.M)
    return t
# Convert MD to HTML
for f in md_parts:
    html_parts[re.sub(r'\.md', '.html', f)] = mdToHTML(md_parts[f])
for f in md_patterns:
    html_patterns[re.sub(r'\.md', '.html', f)] = mdToHTML(md_patterns[f])
for f in md_pages:
    html_pages[re.sub(r'\.md', '.html', f)] = mdToHTML(md_pages[f])
for f in md_posts:
    html_posts[re.sub(r'\.md', '.html', f)] = mdToHTML(md_posts[f])
# Build HTML pages
for f in html_parts:
    # only mix in parts
    html_parts[f]['body'] = mixParts(html_parts[f]['body'])
for f in html_patterns:
    # 1) mix in parts, 2) mix pattern
    print('Pre mixParts (h-pt):', f, html_patterns[f])
    html_patterns[f]['body'] = mixParts(html_patterns[f]['body'])
    print('Post mixParts (h-pt):', f, html_patterns[f])
    if 'pattern' in html_patterns[f]:
        html_patterns[f]['body'] = mixPatterns(html_patterns[f]['body'], html_patterns[f]['pattern'])
        print('Post mixPatterns (h-pt):', f, html_patterns[f])
for f in html_pages:
    # 1) mix in parts, 2) replace variables, 3) mix pattern, 4) output to file
    print('Pre mixParts (h-pg):', f, html_pages[f])
    html_pages[f]['body'] = mixParts(html_pages[f]['body'])
    print('Post mixParts (h-pg):', f, html_pages[f])
    if 'pattern' in html_pages[f]:
        html_pages[f]['body'] = mixPatterns(html_pages[f]['body'], html_pages[f]['pattern'])
        print('Post mixPatterns (h-pg):', f, html_pages[f])
    html_pages[f]['body'] = replVars(html_pages[f]['body'], html_pages[f])
    print('Post replVars (h-pg):', f, html_pages[f])
    if f not in config['exclude']:
        with open(os.path.join('./public/', re.sub(r'^pages/', '', f)), 'w') as file:
            file.write(html_pages[f]['body'])
            file.close()
for f in html_posts:
    # 1) mix in parts, 2) replace variables, 3) mix pattern, 4) output to file
    html_posts[f]['body'] = mixParts(html_posts[f]['body'])
    html_posts[f]['body'] = replVars(html_posts[f]['body'], html_posts[f])
    if 'pattern' in html_posts[f]:
        html_posts[f]['body'] = mixPatterns(html_posts[f]['body'], html_posts[f]['pattern'])
    if f not in config['exclude']:
        with open(os.path.join('./public/', f), 'w') as file:
            file.write(html_posts[f]['body'])
            file.close()
for f in assets:
    d = os.path.join('./public/', f)
    Path(re.sub(r'[^/]+$', '', d)).mkdir(parents=True, exist_ok=True)
    shutil.copyfile(f, d)
# Build CMS
with open('./public/admin/index.html', 'w') as f:
    print(fileContents['admin/index.html'], file=f)
if config['netlifycms'] == True:
    os.mkdir('./public/admin/blog')
    with open('./public/admin/blog/index.html', 'w') as f:
        print(fileContents['admin/blog/index.html'], file=f)
    with open('./public/admin/blog/config.yml', 'w') as f:
        print(yaml.dump(config['blog']), file=f)
if config['grapesjs'] == True:
    os.mkdir('./public/admin/editor')
    with open('./public/admin/editor/index.html', 'w') as f:
        print(fileContents['admin/editor/index.html'], file=f)
    with open('./public/admin/editor/config.json', 'w') as f:
        print(json.dumps(config['cms']), file=f)
for i in config['include']:
    d = os.path.join('./public/', re.sub(r'^(pages|assets|patterns|)/', '', i))
    Path(re.sub(r'[^/]+$', '', d)).mkdir(parents=True, exist_ok=True)
    shutil.copyfile(i, d)
