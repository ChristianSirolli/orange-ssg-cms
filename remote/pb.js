// PeanutButterJS, works like glue
function $(q) {
    var el = [];
    if (!q) {
        console.warn('Ummm...', q);
    } else if (typeof q === 'string') {
        if (q.search('^<.{1,}>$') > -1) {
            var t = document.createElement('template');
            t.innerHTML = q;
            el.push(...t.content.childNodes);
        } else if (q.search(/^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/) > -1) {
            el.push(...document.querySelectorAll(/^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/.exec(q)[0]));
        }
    } else if (nodes instanceof NodeList || nodes instanceof HTMLCollection || Array.isArray(q)) {
        el.push(...q);
    } else if (Node.prototype.isPrototypeOf(q)) {
        el.push(q);
    } else {
        console.warn('q didn\'t match known selectors?', q, typeof q === 'string');
    }
    el.append = function (nodes) {
        if (nodes instanceof NodeList || nodes instanceof HTMLCollection || Array.isArray(nodes)) {
            for (var i = 0; i < el.length; i++) {
                var renewNodes = [];
                for (var j = 0; j < nodes.length; j++) {
                    renewNodes.push(nodes[j].cloneNode(true));
                    el[i].appendChild(nodes[j]);
                    nodes[j] = renewNodes[i];
                }
            }
        } else if (typeof nodes === "string") {
            el.append($(nodes));
        } else {
            try {
                for (var i = 0; i < el.length; i++) {
                    var renewNodes = nodes.cloneNode(true);
                    el[i].appendChild(nodes);
                    nodes = renewNodes;
                }
            } catch (e) { }
        }
    }
    el.addClass = function (cn) {
        for (var i = 0; i < el.length; i++) {
            el[i].classList.add(cn);
        }
    }
    el.delClass = function (cn) {
        for (var i = 0; i < el.length; i++) {
            el[i].classList.remove(cn);
        }
    }
    el.on = function (type, handler) {
        var args = [...arguments];
        for (var i = 0; i < el.length; i++) {
            el[i].addEventListener(...args);
        }
    }
    el.css = function (css) {
        for (var i = 0; i < el.length; i++) {
            el[i].style.cssText += css;
        }
    }
    if () {}
    return el;
}
$.getScript = function (url) {
    return new Promise((rs, err) => {
        var r = document.querySelector('script');
        var s = document.createElement('script');
        s.src = url;
        s.onreadystatechange = rs;
        s.onload = rs;
        s.onerror = err;
        r.parentNode.insertBefore(s, r);
    });
}
$.getStyleSheet = function (url) {
    $('head').append(`<link href="${url}" type="text/css" rel="stylesheet" media="screen,print">`);
}
$.loadCSS = function (styles) {
    $('head').append(`<style>${styles}</style>`);
}
$.fileReader = file => {
  return new Promise((resolve, reject) => {
    var fr = new FileReader();  
    fr.onload = resolve;
    fr.readAsDataURL(file);
  });
}
