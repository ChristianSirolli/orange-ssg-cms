const CMS = {
    config: {
        "path": "",
        "ignore": [],
        "debugLvl": "error",
        "gitType": 'gitlab',
        "masterBranch": 'master',
        "devBranch": "orange-dev"
    },
    fileStructure: []
};
$('body').append('<div data-netlify-identity-menu class="cms-theme"></div>')
$('body').append('<div id="cms-publish" class="hidden cms-theme">Publish</div>')
$('body').append('<div id="cms-pages" class="hidden">Pages<br></div>')
$('body').append('<div id="gjs" class="hidden"></div>');
CMS.catch = function (e) {
    try {
        switch (CMS.config.debugLvl) {
            case 'log':
                console.log("Oops:", e);
                break;
            case 'warn':
                console.warn("Oops:", e);
                break;
            case 'error':
                console.error("Oops:", e);
                break;
            default:
                console.error("Oops:", e);
        }
    } catch (err) {
        console.error("/!\\ Oops:", e, '\n/!\\ Then another error occurred after the above one:', err);
    }
}
CMS.init = function () {
    fetch('./config.json').then(response => {
        return response.json();
    }).then(data => {
        CMS.git = new Git(CMS.config.gitType, CMS.config.debugLvl);
        CMS.config = { ...CMS.config, ...data };
        CMS.config.ignore.push(CMS.config.path + '/admin');
        CMS.config.ignoreStr = '(' + CMS.config.ignore.join('|') + ')';
        CMS.git.getBranch(CMS.config.devBranch).then(response => {
            if (response.status === 404) {
                CMS.git.makeBranch(CMS.config.devBranch, CMS.config.masterBranch);
            }
        });
        $('#cms-publish').delClass('hidden');
        $('#cms-publish').on('click', (ev) => {
            CMS.git.makeMerge(CMS.config.devBranch, CMS.config.masterBranch, "Publishing website via OrangeCMS & GrapesJS");
        });
    }).then(() => {
        CMS.git.getTree(CMS.config.devBranch).then(data => {
            data.forEach(i => {
                if (i.type === 'blob') {
                    CMS.genPageBtn(i.path);
                }
            });
        }).catch(CMS.catch);
    }).catch(e => {
        console.warn("Possibly bad config, check following error to be sure:", e);
    });
}
CMS.genPageBtn = function (p) {
    $('#cms-pages').delClass('hidden');
    if (!CMS.config.ignore.includes(p) && p.search('^' + CMS.config.ignoreStr) === -1 && p.startsWith(CMS.config.path)) {
        CMS.git.getFile(CMS.config.devBranch, p).then(file => {
            CMS.fileStructure.push(file);
            el = $('<div class="cms-theme">' + p.split(CMS.config.path)[1] + '</div>');
            el.on('click', ev => CMS.initGrapes(ev, file));
            $('#cms-pages').append(el);
        }).catch(CMS.catch);
    }
}
CMS.initGrapes = function (ev, file) {
    $('#gjs').delClass('hidden');
    var comp;
    var mdConverter = new showdown.Converter();
    if (file.metadata.file_name.match('\.html$') > -1) {
        try {
            comp = [...file.raw.matchAll(/<body[\s\S]*?>([\s\S]+)<\/body>/gm)][0][1];
        } catch (e) {
            comp = [...file.raw.matchAll(/~~~[\s\S]*?~~~([\s\S]+)/gm)][0][1];
        }
    } else if (file.metadata.file_name.match('\.md$') > -1) {
        var md = [...file.raw.matchAll(/~~~[\s\S]*?~~~([\s\S]+)/gm)][0][1];
        comp = mdConverter.makeHtml(md);
    }
    const editor = grapesjs.init({
        container: '#gjs',
        // fromElement: false,
        components: comp,
        height: '70vh',
        width: '100%',
        showOffsets: 1,
        assetManager: {
            embedAsBase64: 1,
        },
        selectorManager: { componentFirst: true },
        styleManager: { clearProperties: 1 },
        storageManager: {
            type: 'git-gateway',
            autosave: true,
            autoload: true,
            stepsBeforeSave: 5,
        },
        panels: { defaults: [] },
        plugins: [
            'grapesjs-lory-slider',
            'grapesjs-tabs',
            'grapesjs-custom-code',
            'grapesjs-touch',
            'grapesjs-parser-postcss',
            'grapesjs-tooltip',
            'grapesjs-tui-image-editor',
            'grapesjs-typed',
            'grapesjs-style-bg',
            'gjs-preset-webpage',
            'grapesjs-style-filter',
            'gjs-plugin-ckeditor'
        ],
        pluginsOpts: {
            'grapesjs-lory-slider': {
                sliderBlock: {
                    category: 'Extra'
                }
            },
            'grapesjs-tabs': {
                tabsBlock: {
                    category: 'Extra'
                }
            },
            'grapesjs-typed': {
                block: {
                    category: 'Extra',
                    content: {
                        type: 'typed',
                        'type-speed': 40,
                        strings: [
                            'Text row one',
                            'Text row two',
                            'Text row three',
                        ],
                    }
                }
            },
            'grapesjs-style-gradient': {
                colorPicker: 'default',
                grapickOpts: {
                    min: 1,
                    max: 99,
                }
            },
            'gjs-preset-webpage': {
                modalImportTitle: 'Import Template',
                modalImportLabel: '<div style="margin-bottom: 10px; font-size: 13px;">Paste here your HTML/CSS and click Import</div>',
                modalImportContent: function (editor) {
                    return editor.getHtml() + '<style>' + editor.getCss() + '</style>'
                },
                filestackOpts: null, //{ key: 'AYmqZc2e8RLGLE7TGkX3Hz' },
                aviaryOpts: false,
                blocksBasicOpts: { flexGrid: 1 },
                customStyleManager: [{
                    name: 'General',
                    buildProps: ['float', 'display', 'position', 'top', 'right', 'left', 'bottom'],
                    properties: [{
                        name: 'Alignment',
                        property: 'float',
                        type: 'radio',
                        defaults: 'none',
                        list: [
                            { value: 'none', className: 'fa fa-times' },
                            { value: 'left', className: 'fa fa-align-left' },
                            { value: 'right', className: 'fa fa-align-right' }
                        ],
                    }, { property: 'position', type: 'select' }],
                }, {
                    name: 'Dimension',
                    open: false,
                    buildProps: ['width', 'flex-width', 'height', 'max-width', 'min-height', 'margin', 'padding'],
                    properties: [{
                        id: 'flex-width',
                        type: 'integer',
                        name: 'Width',
                        units: ['px', '%'],
                        property: 'flex-basis',
                        toRequire: 1,
                    }, {
                        property: 'margin',
                        properties: [
                            { name: 'Top', property: 'margin-top' },
                            { name: 'Right', property: 'margin-right' },
                            { name: 'Bottom', property: 'margin-bottom' },
                            { name: 'Left', property: 'margin-left' }
                        ],
                    }, {
                        property: 'padding',
                        properties: [
                            { name: 'Top', property: 'padding-top' },
                            { name: 'Right', property: 'padding-right' },
                            { name: 'Bottom', property: 'padding-bottom' },
                            { name: 'Left', property: 'padding-left' }
                        ],
                    }],
                }, {
                    name: 'Typography',
                    open: false,
                    buildProps: ['font-family', 'font-size', 'font-weight', 'letter-spacing', 'color', 'line-height', 'text-align', 'text-decoration', 'text-shadow'],
                    properties: [
                        { name: 'Font', property: 'font-family' },
                        { name: 'Weight', property: 'font-weight' },
                        { name: 'Font color', property: 'color' },
                        {
                            property: 'text-align',
                            type: 'radio',
                            defaults: 'left',
                            list: [
                                { value: 'left', name: 'Left', className: 'fa fa-align-left' },
                                { value: 'center', name: 'Center', className: 'fa fa-align-center' },
                                { value: 'right', name: 'Right', className: 'fa fa-align-right' },
                                { value: 'justify', name: 'Justify', className: 'fa fa-align-justify' }
                            ],
                        }, {
                            property: 'text-decoration',
                            type: 'radio',
                            defaults: 'none',
                            list: [
                                { value: 'none', name: 'None', className: 'fa fa-times' },
                                { value: 'underline', name: 'underline', className: 'fa fa-underline' },
                                { value: 'line-through', name: 'Line-through', className: 'fa fa-strikethrough' }
                            ],
                        }, {
                            property: 'text-shadow',
                            properties: [
                                { name: 'X position', property: 'text-shadow-h' },
                                { name: 'Y position', property: 'text-shadow-v' },
                                { name: 'Blur', property: 'text-shadow-blur' },
                                { name: 'Color', property: 'text-shadow-color' }
                            ],
                        }],
                }, {
                    name: 'Decorations',
                    open: false,
                    buildProps: ['opacity', 'border-radius', 'border', 'box-shadow', 'background-bg', 'filter'],
                    properties: [{
                        type: 'slider',
                        property: 'opacity',
                        defaults: 1,
                        step: 0.01,
                        max: 1,
                        min: 0,
                    }, {
                        property: 'border-radius',
                        properties: [
                            { name: 'Top', property: 'border-top-left-radius' },
                            { name: 'Right', property: 'border-top-right-radius' },
                            { name: 'Bottom', property: 'border-bottom-left-radius' },
                            { name: 'Left', property: 'border-bottom-right-radius' }
                        ],
                    }, {
                        property: 'box-shadow',
                        properties: [
                            { name: 'X position', property: 'box-shadow-h' },
                            { name: 'Y position', property: 'box-shadow-v' },
                            { name: 'Blur', property: 'box-shadow-blur' },
                            { name: 'Spread', property: 'box-shadow-spread' },
                            { name: 'Color', property: 'box-shadow-color' },
                            { name: 'Shadow type', property: 'box-shadow-type' }
                        ],
                    }, {
                        id: 'background-bg',
                        property: 'background',
                        type: 'bg',
                    }, {
                        name: 'Filter',
                        property: 'filter',
                        type: 'filter',
                        full: 1,
                    }],
                }, {
                    name: 'Extra',
                    open: false,
                    buildProps: ['transition', 'perspective', 'transform'],
                    properties: [{
                        property: 'transition',
                        properties: [
                            { name: 'Property', property: 'transition-property' },
                            { name: 'Duration', property: 'transition-duration' },
                            { name: 'Easing', property: 'transition-timing-function' }
                        ],
                    }, {
                        property: 'transform',
                        properties: [
                            { name: 'Rotate X', property: 'transform-rotate-x' },
                            { name: 'Rotate Y', property: 'transform-rotate-y' },
                            { name: 'Rotate Z', property: 'transform-rotate-z' },
                            { name: 'Scale X', property: 'transform-scale-x' },
                            { name: 'Scale Y', property: 'transform-scale-y' },
                            { name: 'Scale Z', property: 'transform-scale-z' }
                        ],
                    }]
                }, {
                    name: 'Flex',
                    open: false,
                    properties: [{
                        name: 'Flex Container',
                        property: 'display',
                        type: 'select',
                        defaults: 'block',
                        list: [
                            { value: 'block', name: 'Disable' },
                            { value: 'flex', name: 'Enable' }
                        ],
                    }, {
                        name: 'Flex Parent',
                        property: 'label-parent-flex',
                        type: 'integer',
                    }, {
                        name: 'Direction',
                        property: 'flex-direction',
                        type: 'radio',
                        defaults: 'row',
                        list: [{
                            value: 'row',
                            name: 'Row',
                            className: 'icons-flex icon-dir-row',
                            title: 'Row',
                        }, {
                            value: 'row-reverse',
                            name: 'Row reverse',
                            className: 'icons-flex icon-dir-row-rev',
                            title: 'Row reverse',
                        }, {
                            value: 'column',
                            name: 'Column',
                            title: 'Column',
                            className: 'icons-flex icon-dir-col',
                        }, {
                            value: 'column-reverse',
                            name: 'Column reverse',
                            title: 'Column reverse',
                            className: 'icons-flex icon-dir-col-rev',
                        }],
                    }, {
                        name: 'Justify',
                        property: 'justify-content',
                        type: 'radio',
                        defaults: 'flex-start',
                        list: [{
                            value: 'flex-start',
                            className: 'icons-flex icon-just-start',
                            title: 'Start',
                        }, {
                            value: 'flex-end',
                            title: 'End',
                            className: 'icons-flex icon-just-end',
                        }, {
                            value: 'space-between',
                            title: 'Space between',
                            className: 'icons-flex icon-just-sp-bet',
                        }, {
                            value: 'space-around',
                            title: 'Space around',
                            className: 'icons-flex icon-just-sp-ar',
                        }, {
                            value: 'center',
                            title: 'Center',
                            className: 'icons-flex icon-just-sp-cent',
                        }],
                    }, {
                        name: 'Align',
                        property: 'align-items',
                        type: 'radio',
                        defaults: 'center',
                        list: [{
                            value: 'flex-start',
                            title: 'Start',
                            className: 'icons-flex icon-al-start',
                        }, {
                            value: 'flex-end',
                            title: 'End',
                            className: 'icons-flex icon-al-end',
                        }, {
                            value: 'stretch',
                            title: 'Stretch',
                            className: 'icons-flex icon-al-str',
                        }, {
                            value: 'center',
                            title: 'Center',
                            className: 'icons-flex icon-al-center',
                        }],
                    }, {
                        name: 'Flex Children',
                        property: 'label-parent-flex',
                        type: 'integer',
                    }, {
                        name: 'Order',
                        property: 'order',
                        type: 'integer',
                        defaults: 0,
                        min: 0
                    }, {
                        name: 'Flex',
                        property: 'flex',
                        type: 'composite',
                        properties: [{
                            name: 'Grow',
                            property: 'flex-grow',
                            type: 'integer',
                            defaults: 0,
                            min: 0
                        }, {
                            name: 'Shrink',
                            property: 'flex-shrink',
                            type: 'integer',
                            defaults: 0,
                            min: 0
                        }, {
                            name: 'Basis',
                            property: 'flex-basis',
                            type: 'integer',
                            units: ['px', '%', ''],
                            unit: '',
                            defaults: 'auto',
                        }],
                    }, {
                        name: 'Align',
                        property: 'align-self',
                        type: 'radio',
                        defaults: 'auto',
                        list: [{
                            value: 'auto',
                            name: 'Auto',
                        }, {
                            value: 'flex-start',
                            title: 'Start',
                            className: 'icons-flex icon-al-start',
                        }, {
                            value: 'flex-end',
                            title: 'End',
                            className: 'icons-flex icon-al-end',
                        }, {
                            value: 'stretch',
                            title: 'Stretch',
                            className: 'icons-flex icon-al-str',
                        }, {
                            value: 'center',
                            title: 'Center',
                            className: 'icons-flex icon-al-center',
                        }],
                    }]
                }],
            },
        },
    });
    CMS.editor = editor;
    CMS.GitGateway = {};
    editor.on('storage:error', CMS.catch);
    editor.StorageManager.add('git-gateway', {
        /**
         * Convert Git file to GrapeJS keys
         * @param  {Array} keys Array containing values to load, eg, ['gjs-components', 'gjs-style', ...]
         * @param  {Function} clb Callback function to call when the load is ended
         * @param  {Function} clbErr Callback function to call in case of errors
         */
        load(keys, clb, clbErr) {
            try {
                CMS.git.getFile(CMS.config.devBranch, CMS.config.path + ev.target.innerText).then(file => {
                    const result = {};
                    if (file.metadata.file_name.match('\.html$') > -1) {
                        try {
                            result['gjs-html'] = [...file.raw.matchAll(/<body[\s\S]*?>([\s\S]+)<\/body>/gm)][0][1];
                        } catch (e) {
                            result['gjs-html'] = [...file.raw.matchAll(/~~~[\s\S]*?~~~([\s\S]+)/gm)][0][1];
                        }
                    } else if (file.metadata.file_name.match('\.md$') > -1) {
                        var md = [...file.raw.matchAll(/~~~[\s\S]*?~~~([\s\S]+)/gm)][0][1];
                        result['gjs-html'] = mdConverter.makeHtml(md);
                    }
                    var fm = JSON.parse([...file.raw.matchAll(/~~~\n([\s\S]*?)\n~~~/gm)][0][1]);
                    result['gjs-components'] = fm['gjs-components'];
                    result['gjs-assets'] = fm['gjs-assets'];
                    result['gjs-css'] = fm['gjs-css'];
                    result['gjs-styles'] = fm['gjs-styles'];
                    return result;
                }).then(clb).catch(clbErr);
            } catch (e) {
                clbErr(e);
            }
        },

        /**
         * Convert GrapeJS keys to Git commits
         * @param  {Object} data Data object to store
         * @param  {Function} clb Callback function to call when the load is ended
         * @param  {Function} clbErr Callback function to call in case of errors
         */
        store(data, clb, clbErr) {
            try {
                var fm = {
                    'gjs-components': data['gjs-components'],
                    'gjs-assets': data['gjs-assets'],
                    'gjs-css': data['gjs-css'],
                    'gjs-styles': data['gjs-styles']
                }
                CMS.git.getFile(CMS.config.devBranch, CMS.config.path + ev.target.innerText).then(file => {
                    var f;
                    if (file.metadata.file_name.match('\.html$') > -1) {
                        try {
                            f = file.raw.replace(/(<body[\s\S]*?>)([\s\S]+)(<\/body>)/gm, function (a, b, c, d) {
                                return b + data['gjs-html'] + d;
                            }).replace(/(~~~)([\s\S]*?)(~~~)/gm, function (a, b, c, d) {
                                return b + JSON.stringify({ ...JSON.parse(c), ...fm }) + d;
                            });
                        } catch (e) {
                            f = file.raw.replace(/(~~~[\s\S]*?~~~)([\s\S]+)/gm, function (a, b, c) {
                                return b + data['gjs-html'];
                            }).replace(/(~~~)([\s\S]*?)(~~~)/gm, function (a, b, c, d) {
                                return b + JSON.stringify({ ...JSON.parse(c), ...fm }) + d;
                            });
                        }
                    } else if (file.metadata.file_name.match('\.md$') > -1) {
                        f = file.raw.replace(/(<body[\s\S]*?>)([\s\S]+)(<\/body>)/gm, function (a, b, c, d) {
                            return b + mdConverter.makeMarkdown(data['gjs-html']) + d;
                        }).replace(/(~~~)([\s\S]*?)(~~~)/gm, function (a, b, c, d) {
                            return b + JSON.stringify({ ...JSON.parse(c), ...fm }) + d;
                        });
                    }
                    return CMS.git.makeCommits(CMS.config.devBranch, CMS.config.path + ev.target.innerText, {
                        "branch": CMS.config.devBranch,
                        "commit_message": "(GrapesJS) Saving " + CMS.config.path + ev.target.innerText,
                        "actions": [
                            {
                                "action": "update",
                                "file_path": CMS.config.path + ev.target.innerText,
                                "content": f,
                            },
                        ]
                    });
                }).then(clb).catch(clbErr);
                // Might be called inside some async method
                clb();
            } catch (e) {
                clbErr(e);
            }
        }
    });
    editor.I18n.addMessages({
        en: {
            styleManager: {
                properties: {
                    'background-repeat': 'Repeat',
                    'background-position': 'Position',
                    'background-attachment': 'Attachment',
                    'background-size': 'Size',
                }
            },
        }
    });
    var pn = editor.Panels;
    var modal = editor.Modal;
    var cmdm = editor.Commands;
    cmdm.add('canvas-clear', function () {
        if (confirm('Are you sure to clean the canvas?')) {
            var comps = editor.DomComponents.clear();
            setTimeout(function () { localStorage.clear() }, 0)
        }
    });
    cmdm.add('set-device-desktop', {
        run: function (ed) { ed.setDevice('Desktop') },
        stop: function () { },
    });
    cmdm.add('set-device-tablet', {
        run: function (ed) { ed.setDevice('Tablet') },
        stop: function () { },
    });
    cmdm.add('set-device-mobile', {
        run: function (ed) { ed.setDevice('Mobile portrait') },
        stop: function () { },
    });
    // Add info command
    var mdlClass = 'gjs-mdl-dialog-sm';
    var infoContainer = document.getElementById('info-panel');
    cmdm.add('open-info', function () {
        var mdlDialog = document.querySelector('.gjs-mdl-dialog');
        mdlDialog.className += ' ' + mdlClass;
        infoContainer.style.display = 'block';
        modal.setTitle('About this demo');
        modal.setContent(infoContainer);
        modal.open();
        modal.getModel().once('change:open', function () {
            mdlDialog.className = mdlDialog.className.replace(mdlClass, '');
        })
    });
    pn.addButton('options', {
        id: 'open-info',
        className: 'fa fa-question-circle',
        command: function () { editor.runCommand('open-info') },
        attributes: {
            'title': 'About',
            'data-tooltip-pos': 'bottom',
        },
    });
    // Simple warn notifier
    var origWarn = console.warn;
    toastr.options = {
        closeButton: true,
        preventDuplicates: true,
        showDuration: 250,
        hideDuration: 150
    };
    console.warn = function (msg) {
        if (msg.indexOf('[undefined]') == -1) {
            toastr.warning(msg);
        }
        origWarn(msg);
    };
    // Add and beautify tooltips
    [['sw-visibility', 'Show Borders'], ['preview', 'Preview'], ['fullscreen', 'Fullscreen'],
    ['export-template', 'Export'], ['undo', 'Undo'], ['redo', 'Redo'],
    ['gjs-open-import-webpage', 'Import'], ['canvas-clear', 'Clear canvas']]
        .forEach(function (item) {
            pn.getButton('options', item[0]).set('attributes', { title: item[1], 'data-tooltip-pos': 'bottom' });
        });
    [['open-sm', 'Style Manager'], ['open-layers', 'Layers'], ['open-blocks', 'Blocks']]
        .forEach(function (item) {
            pn.getButton('views', item[0]).set('attributes', { title: item[1], 'data-tooltip-pos': 'bottom' });
        });
    var titles = document.querySelectorAll('*[title]');
    for (var i = 0; i < titles.length; i++) {
        var el = titles[i];
        var title = el.getAttribute('title');
        title = title ? title.trim() : '';
        if (!title)
            break;
        el.setAttribute('data-tooltip', title);
        el.setAttribute('title', '');
    }
    // Show borders by default
    pn.getButton('options', 'sw-visibility').set('active', 1);
    // Store and load events
    editor.on('storage:load', function (e) { console.log('Loaded ', e) });
    editor.on('storage:store', function (e) { console.log('Stored ', e) });
    // Do stuff on load
    editor.on('load', function () {
        var $ = grapesjs.$;
        // Load and show settings and style manager
        var openTmBtn = pn.getButton('views', 'open-tm');
        openTmBtn && openTmBtn.set('active', 1);
        var openSm = pn.getButton('views', 'open-sm');
        openSm && openSm.set('active', 1);
        // Add Settings Sector
        var traitsSector = $('<div class="gjs-sm-sector no-select">' +
            '<div class="gjs-sm-title"><span class="icon-settings fa fa-cog"></span> Settings</div>' +
            '<div class="gjs-sm-properties" style="display: none;"></div></div>');
        var traitsProps = traitsSector.find('.gjs-sm-properties');
        traitsProps.append($('.gjs-trt-traits'));
        $('.gjs-sm-sectors').before(traitsSector);
        traitsSector.find('.gjs-sm-title').on('click', function () {
            var traitStyle = traitsProps.get(0).style;
            var hidden = traitStyle.display == 'none';
            if (hidden) {
                traitStyle.display = 'block';
            } else {
                traitStyle.display = 'none';
            }
        });
        // Open block manager
        var openBlocksBtn = editor.Panels.getButton('views', 'open-blocks');
        openBlocksBtn && openBlocksBtn.set('active', 1);
    });
}
window.onload = () => {
    if (netlifyIdentity.currentUser() === null) {
        netlifyIdentity.open();
    }
};
netlifyIdentity.on('login', user => {
    netlifyIdentity.close();
    CMS.init();
});
netlifyIdentity.on('logout', user => {
    document.location.reload();
});
netlifyIdentity.on('close', user => {
    if (netlifyIdentity.currentUser() === null) {
        netlifyIdentity.open();
    }
});
var scripts = [
    'https://orange-ssg-cms.netlify.com/gitgateway.js',
    'https://unpkg.com/grapesjs',
    'https://cdn.ckeditor.com/4.14.0/full-all/ckeditor.js',
    'https://unpkg.com/grapesjs-plugin-ckeditor',
    'https://unpkg.com/grapesjs-typed',
    'https://unpkg.com/grapesjs-tui-image-editor',
    'https://unpkg.com/grapesjs-preset-webpage',
    'https://unpkg.com/grapesjs-style-gradient',
    'https://unpkg.com/grapesjs-style-filter',
    'https://unpkg.com/grapesjs-style-bg',
    'https://unpkg.com/grapesjs-lory-slider',
    'https://unpkg.com/grapesjs-tabs',
    'https://unpkg.com/grapesjs-tooltip',
    'https://unpkg.com/grapesjs-custom-code',
    'https://unpkg.com/grapesjs-touch',
    'https://unpkg.com/grapesjs-parser-postcss',
    'https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js',
    'https://unpkg.com/showdown/dist/showdown.min.js',
];
var stylesheets = [
    'https://unpkg.com/grapesjs/dist/css/grapes.min.css',
    'https://unpkg.com/grapesjs-preset-webpage/dist/grapesjs-preset-webpage.min.css',
    'https://unpkg.com/grapick/dist/grapick.min.css',
    'https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.css',
]
scripts.forEach(s => {
    $.getScript(s).catch(CMS.catch);
});
stylesheets.forEach(s => {
    $.getStyleSheet(s);
});
$.loadCSS(`body {background-color: #463a3c;}
    div[data-netlify-identity-menu] {cursor: default;}
    .netlify-identity-menu {list-style-type: none;display: inline-block;margin: 0;padding: 0;}
    .netlify-identity-item > a {color: #b9a5a6;}
    #cms-pages {display: block;font-family: Helvetica,sans-serif;font-size: 12px;color: #b9a5a6;}
    .cms-theme {border: 1px solid rgba(0,0,0,0.2);border-radius: 3px;box-shadow: 0 1px 0 0 rgba(0,0,0,0.15);background-color: #463a3c;color: #b9a5a6;margin: 2px;padding: 1em;display: inline-block;cursor: pointer;font-family: Helvetica,sans-serif;font-size: 12px;user-select: none;}
    #cms-pages > div {display: inline-block;}
    #cms-publish {position: absolute;right: 0;top: 0;width: 100px;height: 20px;line-height: 20px;text-align: center;display: block;}
    #gjs {border: 1px solid rgba(0,0,0,0.2);border-radius: 3px;box-shadow: 0 1px 0 0 rgba(0,0,0,0.15);}
    .gjs-cv-canvas {width: 100%;max-height: 100%;}
    .hidden {display: none !important;}`);
/* Licenses

*/
