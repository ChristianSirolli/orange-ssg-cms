window.onload = () => {
    if (netlifyIdentity.currentUser() === null) {
        netlifyIdentity.open();
    } else {
        $('body').append('<h1>Dashboard</h1>\n<p></p>\n<ul id="cms-nav"></ul>');
        $('#cms-nav').append('<li><a href="./editor/" target="_blank">Edit Website Pages</a></li>');
        $('#cms-nav').append('<li><a href="./blog/" target="_blank">Edit Blog Posts</a></li>');
    }
};
netlifyIdentity.on('login', user => {
    netlifyIdentity.close();
    $('#cms-nav').append('<li><a href="./editor/" target="_blank">Edit Website Pages</a></li>');
    $('#cms-nav').append('<li><a href="./blog/" target="_blank">Edit Blog Posts</a></li>');
});
netlifyIdentity.on('logout', user => {
    document.location.reload();
});
netlifyIdentity.on('close', user => {
    if (netlifyIdentity.currentUser() === null) {
        netlifyIdentity.open();
    }
});
