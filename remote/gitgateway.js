// Simple functions for interacting with Git Gateway
class Git {
    constructor(gitType, debugLvl) {
        this.debugLvl = debugLvl !== undefined ? debugLvl : 'error';
        this.cu = netlifyIdentity.currentUser();
        if (this.cu === null) {
            netlifyIdentity.open();
            netlifyIdentity.on('login', () => {
                this.cu = netlifyIdentity.currentUser();
            });
        }
        this.token = this.cu.token;
        this.headers = { 'Authorization': this.token.token_type + ' ' + this.token.access_token, 'Content-Type': 'application/json' };
        this.gitType = gitType.toLowerCase();
        if (this.gitType === 'gitlab') {
            this.url = '/.netlify/git/gitlab/repository/';
        } else if (this.gitType === 'gitlab') {
            this.url = '/.netlify/git/github/';
        }
    }
    catch(e, $this) {
        try {
            switch ($this.debugLvl) {
                case 'log':
                    console.log("Git error:", e);
                    break;
                case 'warn':
                    console.warn("Git error:", e);
                    break;
                case 'error':
                    console.error("Git error:", e);
                    break;
                default:
                    console.error("Git error:", e);
            }
        } catch (err) {
            console.error("/!\\ Git error:", e, '\n/!\\ Then another error occurred after the above one:', err);
        }
    }
    getFile(branch, path) {
        var $this = this;
        if (this.gitType === 'gitlab') {
            var p = path.replace(/\//g, '%2F');
            var f = {};
            return fetch(this.url + 'files/' + p + '?ref=' + branch, { headers: this.headers }).then(response => {
                return response.json();
            }).then(data => {
                f.metadata = data;
                return fetch(url + 'files/' + p + '/raw?ref=' + branch, { headers: this.headers });
            }).then(response => {
                return response.text();
            }).then(data => {
                f.raw = data;
                return f;
            }).catch((e) => this.catch(e, $this));
        } else if (this.gitType === 'github') {
            var f = {
                metadata: {},
                raw: ''
            };
            return new Promise((r1, r2) => {
                return f;
            });
        }
    }
    makeCommits(body) {
        if (typeof body === "string") {
            body = JSON.parse('body');
        }
        var $this = this;
        if (this.gitType === 'gitlab') {
            return fetch(this.url + 'commits/', { method: 'post', headers: this.headers, body: JSON.stringify(body) }).then(response => {
                return response;
            }).catch((e) => this.catch(e, $this));
        } else if (this.gitType === 'github') {
            url += 'contents/';
        }
    }
    getBranch(branch) {
        var $this = this;
        if (this.gitType === 'gitlab') {
            return fetch(this.url + 'branches/' + branch, { headers: this.headers }).catch((e) => this.catch(e, $this));
        } else if (this.gitType === 'github') {
            var f = {
                metadata: {},
                raw: ''
            };
            return new Promise((r1, r2) => {
                return f;
            });
        }
    }
    makeBranch(branch, ref) {
        var $this = this;
        if (this.gitType === 'gitlab') {
            return fetch(this.url + 'branches?branch=' + branch + '&ref=' + ref, { method: 'post', headers: this.headers }).catch((e) => this.catch(e, $this));
        } else if (this.gitType === 'github') {
            var f = {
                metadata: {},
                raw: ''
            };
            return new Promise((r1, r2) => {
                return f;
            });
        }
    }
    makeMerge(source_branch, target_branch, title) {
        var $this = this;
        if (this.gitType === 'gitlab') {
            return fetch(this.url + 'merge_requests', {
                method: 'post', headers: this.headers, body: JSON.stringify({ source_branch, target_branch, title })
            }).catch((e) => this.catch(e, $this));
        } else if (this.gitType === 'github') {
            var f = {
                metadata: {},
                raw: ''
            };
            return new Promise((r1, r2) => {
                return f;
            });
        }
    }
    getTree(branch) {
        var $this = this;
        if (this.gitType === 'gitlab') {
            return fetch(this.url + 'tree?ref=' + branch + '&recursive=true', { headers: this.headers }).then(response => {
                if (response.status === 400) {
                    netlifyIdentity.logout().then(() => {
                        document.location.reload();
                    });
                } else {
                    return response.json();
                }
            }).catch((e) => this.catch(e, $this));
        } else if (this.gitType === 'github') {
            var f = {
                metadata: {},
                raw: ''
            };
            return new Promise((r1, r2) => {
                return f;
            });
        }
    }
}
