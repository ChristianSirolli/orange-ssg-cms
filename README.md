# Orange SSG & CMS
Orange is a complete SSG and CMS. GrapesJS is used for editing pages, while Netlify CMS is used for editing blog posts. Behind it all is the Git Gateway. The Netlify CMS implementation is functional and largely complete. The GrapesJS implementation is partially functional and far from complete. Currently, only storing files on GitLab is supported, but GitHub support is planned and is partially present.

## Get Started
Put pages in the `pages` directory. Put static files in the `assets` directory. Put blog posts in the `posts` directory. Put files that may be included in other pages in the `parts` directory.

## Frontmatter
Use `~~~` as a wrapper for frontmatter. Example:

```
~~~
title: Home
~~~
```
This must be at the beginning of the file, else it won't be recognized and may be ignored.

## Partials
Partials are placed in the `parts` directory. You can specify where in a file you want a partial to be loaded with using this format:
```
{{{ header }}}
```
In this example, the `header.html` file in the `parts` directory would be loaded into the file this is found in.

## Posts
It is recommended that you use Netlify CMS to create blog posts. Go to the Orange Dashboard (`yourwebsite.com/admin`) and click on "Edit Blog Posts". This will open a new tab or window taking you to the Netlify CMS where you can easily create and edit blog posts.

## Build
To build the site, connect it to Netlify, turn on Identity and Git Gateway, and you are good to go.

Alternatively, run `build.py` (Python 3) to generate files in a `public` directory, which can be served as is. However, the CMS feature will not work without Netlify and its Identity and Git Gateway features.

## Configuration
The Orange configuration file is `.orange-config.json`. It is a JSON file which can have the following:
* `"url"`: The website's URL.
* `"branch"`: The main Git branch that contains the website files. Netlfiy CMS and GrapesJS will publish files in this branch; however, they may create their own branch to create drafts before the files are published.
* `"git"`: The Git provider. Currently, only GitLab (`"gitlab"`) is working and being focused on. Eventually, GitHub (`"github"`) will be completely supported. Netlify CMS supports both, but GrapesJS does not.
* `"grapesjs"`: Whether or not to include the GrapesJS WYSIWYG editor. The Orange implementation here is plugin rich and uses Netlify's Git Gateway as a storage manager. Is either `true` or `false`.
* `"netlifycms"`: Whether or not to include the Netlify CMS. The Orange implementation is configured specifically to work with this SSG set up. It can be additionally configured with `"blog"`. (See below) Is either `true` or `false`.
* `"blog"`: Is an object (`{...}`) of Netlify CMS configuration values. If the default configuration is not sufficient, add your own configuration here and it will overright the default.
* `"include"`: Is an array of files (with path relative to the repository, without a preceding forward slash) that would not be appropriate to be stored in the `assets` directory, but ought to be served as static files. Some services require uploading a file to the root directory of your site to confirm ownership. You do not need to add a favicon to this array, provided it is named `favicon.ico` or `favicon.png`, as Orange automatically looks for it and adds it appropriately. These files will be checked for frontmatter, and will be processed as a regular file if frontmatter is found. Otherwise, it is served as a static file.
* `"exclude"`: Is an array of files (with path relative to the repository, without a preceding forward slash) that should not be processed by Orange. By default, files beginning with a period (e.g. `.gitignore`) will be excluded and do not need to be added here.

Example:
```
{
    "url": "https://example.com",
    "branch": "master",
    "git": "gitlab",
    "grapesjs": true,
    "netlifycms": true,
    "blog": {
        "publish_mode": "editorial_workflow"
    },
    "include": [],
    "exclude": []
}
```
