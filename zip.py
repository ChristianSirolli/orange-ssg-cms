#!/usr/bin/env python3
import zipfile, os
with zipfile.ZipFile('./remote/orange.zip', 'w') as ozip:
    for f in os.listdir('src'):
        ozip.write(os.path.join('./src/', f), f)
    ozip.close()
